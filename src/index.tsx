
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux'

import store from "./configureStore";
import App from "./App";

const Index = () => {
	return <Provider store={store}>
		<App />
	</Provider>;
};

ReactDOM.render(<Index />, document.getElementById("root"));