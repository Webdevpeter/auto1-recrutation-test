import { configureStore, getDefaultMiddleware  } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';

import merchantReducer from './store/merchantsSlice';
import sagas from './sagas';

export const history = createBrowserHistory();

const createStore = () => {
    const sagaMiddleware = createSagaMiddleware();

    const middlewares = [sagaMiddleware, routerMiddleware(history), logger];
    
    
    const store = configureStore({
        reducer: {
            merchants: merchantReducer,
            router: connectRouter(history),
        },
        middleware: [...getDefaultMiddleware({thunk: false}), ...middlewares],
        devTools: process.env.NODE_ENV !== 'production',
    });
    
    sagaMiddleware.run(sagas);
    
    return store;
}

const store = createStore();


export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;