import dayjs from 'dayjs';

import { Bid } from './store/merchantsSlice';
import { Merchant } from './store/merchantsSlice';

function idGenFactory() {
    let lastId = 200;
    
    return () => {
        return ++lastId;
    }
}

const merchantIdGenerator = idGenFactory();
const bidIdGenerator = idGenFactory();

function getRandomNumber(min: number, max: number) {
    return Math.round(Math.random() * (max - min) + min);
}

const carTitles = ['audi', 'volkswagen', 'mercedes', 'opel', 'lamborghini', 'seat'];

function bidsGenerator(): Bid[] {
    return [...Array(getRandomNumber(2, 20))].map(() => ({
        id: bidIdGenerator().toString(),
        carTitle: carTitles[getRandomNumber(0, carTitles.length - 1)],
        amount: getRandomNumber(1, 999999),
        created: dayjs(`2021-03-${getRandomNumber(1, 31)}`).format(),
    }))
}

const newMerchant = (params: Omit<Merchant, 'id' | 'bids'>): Merchant => {
    const {firstname, lastname, avatarUrl, email, phone, hasPremium} = params;
    return {
        id: merchantIdGenerator().toString(),
        firstname,
        lastname,
        avatarUrl,
        email,
        phone,
        hasPremium,
        bids: bidsGenerator()
    };
}


export { bidIdGenerator, merchantIdGenerator, newMerchant };