import React, { useEffect } from "react";
import {
	Switch,
	Route,
} from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router'

import { useAppDispatch } from './hooks';
import { fetchMerchants } from './store/merchantsSlice';
import { history } from "./configureStore";
import Merchant from "./Views/Merchant";
import Merchants from './Views/Merchants';
import CreateMerchant from './Views/CreateMerchant';

function App(): JSX.Element {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchMerchants())
    }, []);

    return <ConnectedRouter history={history}>
        <Switch>
            <Route exact path='/merchant/create' component={CreateMerchant} />
            <Route exact path='/merchant/:id' component={Merchant} />
            <Route path='/' component={Merchants} />
        </Switch>
    </ConnectedRouter>;
}

export default App;