import { Merchant } from './store/merchantsSlice'
import { newMerchant } from './db-mock';

export function fetchMerchants(count: number = 30): Promise<Merchant[]> {
    return new Promise((resolve) => {
        setTimeout(() => {
            const merchants = [...Array(count)].map(() => {
                return newMerchant({
                    firstname: "test",
                    lastname: "test",
                    avatarUrl: "test",
                    email: "test",
                    phone: "111 111 111",
                    hasPremium: false,
                });
            });
            resolve(merchants)
        }, 3000);
    });
}

export function createMerchant(params: Omit<Merchant, 'id' | 'bids'>): Promise<Merchant> {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(newMerchant({
                ...params
            }));
        });
    });
}