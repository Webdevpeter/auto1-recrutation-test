import React from 'react';
import { Link } from "react-router-dom";
import { push } from 'connected-react-router';
import Button from '@material-ui/core/Button';

import { actionCreateMerchant } from '../../store/merchantsSlice';
import { useAppDispatch } from '../../hooks';

function CreateMerchant() {
    const dispatch = useAppDispatch()

    const firstnameRef = React.useRef<HTMLInputElement>(null);
    const lastnameRef = React.useRef<HTMLInputElement>();
    const emailRef = React.useRef<HTMLInputElement>();
    const phoneRef = React.useRef<HTMLInputElement>();
    const avatarUrlRef = React.useRef<HTMLInputElement>();
    const hasPremiumRef = React.useRef<HTMLInputElement>();

    return <>
        <nav>
            <Link to="/"><Button variant="contained">Back</Button></Link>
        </nav>
        <div>firstname: <input ref={firstnameRef} /></div>
        <div>lastname: <input ref={lastnameRef} /></div>
        <div>email: <input ref={emailRef} /></div>
        <div>phone: <input ref={phoneRef} /></div>
        <div>avatarUrl: <input ref={avatarUrlRef} /></div>
        <div>hasPremium: <input type="checkbox" ref={hasPremiumRef} /></div>

        <Button variant="contained" onClick={() => {
            dispatch(actionCreateMerchant({
                firstname: firstnameRef.current.value,
                lastname: lastnameRef.current.value,
                email: emailRef.current.value,
                phone: phoneRef.current.value,
                avatarUrl: avatarUrlRef.current.value,
                hasPremium: hasPremiumRef.current.checked,
            }));
            dispatch(push('/'));
        }}>Create</Button>
    </>
}

export default CreateMerchant;