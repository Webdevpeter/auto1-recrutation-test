import React from "react";
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';

import MerchantsList from '../../components/MerchantsList';

function Merchants(): JSX.Element {
    return <>
        <nav>
            <Link to="/merchant/create"><Button variant="contained">Create Merchant</Button></Link>
        </nav>
        <h1>Merchants</h1>
        <MerchantsList />
    </>;
}

export default Merchants;