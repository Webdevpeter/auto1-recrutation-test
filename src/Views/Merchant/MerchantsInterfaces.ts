import { RouteComponentProps } from 'react-router-dom';

interface ViewUrlParams {
    id: string;
}

export interface MerchantProps extends RouteComponentProps<ViewUrlParams> {}