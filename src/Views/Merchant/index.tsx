import React, {useState} from 'react';
import dayjs from 'dayjs';
import { find } from 'lodash-es'
import { Link } from "react-router-dom";
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';

import { useAppSelector, useAppDispatch } from '../../hooks';
import { Merchant, updateMerchant } from '../../store/merchantsSlice';
import { MerchantProps } from './MerchantsInterfaces';

function Merchant(props: MerchantProps): JSX.Element {
    const [isEdit, setIsEdit] = useState(false);
    const firstnameRef = React.useRef<HTMLInputElement>(null);
    const lastnameRef = React.useRef<HTMLInputElement>();
    const emailRef = React.useRef<HTMLInputElement>();
    const phoneRef = React.useRef<HTMLInputElement>();
    const avatarUrlRef = React.useRef<HTMLInputElement>();
    const hasPremiumRef = React.useRef<HTMLInputElement>();
    const { id } = props.match.params;
    const merchant = useAppSelector(state => find(state.merchants.merchants, (merchant) => merchant.id === id));
    const dispatch = useAppDispatch();
    if (!merchant) {
        return <>{'Loading...'}</>
    }

    const { firstname, lastname, avatarUrl, email, phone, hasPremium } = merchant;

    return <>
        <nav>
            <Link to="/"><Button variant="contained">Back</Button></Link><Button onClick={() => {setIsEdit(!isEdit)}} variant="contained" color="primary">Edit</Button>
        </nav>
        <h1>Merchant Info</h1>
        <p>id: {id}</p>
        {isEdit ? <>
            <div>firstname: <Input defaultValue={firstname} inputRef={firstnameRef} /></div>
            <div>lastname: <Input defaultValue={lastname} inputRef={lastnameRef} /></div>
            <div>email: <Input defaultValue={avatarUrl} inputRef={emailRef} /></div>
            <div>phone: <Input defaultValue={email} inputRef={phoneRef} /></div>
            <div>avatarUrl: <Input defaultValue={phone} inputRef={avatarUrlRef} /></div>
            <div>hasPremium: <Checkbox defaultChecked={hasPremium} inputRef={hasPremiumRef} /></div>
            <Button onClick={() => {
                dispatch(updateMerchant({
                    ...merchant,
                    firstname: firstnameRef.current.value,
                    lastname: lastnameRef.current.value,
                    email: emailRef.current.value,
                    phone: phoneRef.current.value,
                    avatarUrl: avatarUrlRef.current.value,
                    hasPremium: hasPremiumRef.current.checked,
                }));
                setIsEdit(false);
            }} variant="contained" color="secondary">Save</Button>
        </> : <>
            <p>firstname: {firstname}</p>
            <p>lastname: {lastname}</p>
            <p>avatarUrl: {avatarUrl}</p>
            <p>email: {email}</p>
            <p>phone: {phone}</p>
            <p>hasPremium: {hasPremium.toString()}</p>
        </>}

        <h2>bids:</h2>
        {[...merchant.bids].sort((a, b) => (dayjs(a.created).isAfter(dayjs(b.created)) ? 1 : -1)).map(bid => <div key={`key-${bid.id}`}>
            <div>amount: {bid.amount}</div>
            <div>carTitle: {bid.carTitle}</div>
            <div>created: {bid.created}</div>
            <div>id: {bid.id}</div>
        </div>)}
    </>
}

export default Merchant;