import { createSlice, PayloadAction, createAction } from '@reduxjs/toolkit';

export interface Bid {
    id: string,
    carTitle: string,
    amount: number,
    created: string
}

export interface Merchant {
    id: string,
    firstname: string,
    lastname: string,
    avatarUrl?: string,
    email: string,
    phone: string,
    hasPremium: boolean,
    bids: Array<Bid>
}

interface MerchantState {
    merchants: Merchant[];
    isLoading: boolean;
}

const initialState: MerchantState = {
    isLoading: false,
    merchants: null,
}

export const merchantState = createSlice({
    name: 'merchant',
    initialState,
    reducers: {
        setIsLoading: (state, action: PayloadAction<boolean>) => {state.isLoading = action.payload},
        setMerchants: (state, action: PayloadAction<Merchant[], "@@merchants/FETCH_ALL">) => {state.merchants = action.payload},
        addMerchant: (state, action: PayloadAction<Merchant>) => {state.merchants.push(action.payload)},
        removeMerchant: (state, action: PayloadAction<string>) => {state.merchants = state.merchants.filter((merchant) => merchant.id !== action.payload)},
        updateMerchant: (state, action: PayloadAction<Merchant>) => {state.merchants = state.merchants.map((merchant) => merchant.id === action.payload.id ? action.payload : merchant )},
    }
});

export const fetchMerchants = createAction('@@merchants/FETCH_ALL');
export const actionCreateMerchant = createAction<Omit<Merchant, 'id' | 'bids'>>('@@merchants/CREATE');

export const { setIsLoading, setMerchants, addMerchant, removeMerchant, updateMerchant } = merchantState.actions;
export default merchantState.reducer;