import { takeLatest, call, fork, put } from 'redux-saga/effects';

import { setIsLoading, setMerchants, addMerchant, actionCreateMerchant } from '../store/merchantsSlice';
import { newMerchant } from '../db-mock';
import { fetchMerchants, createMerchant } from '../requests';

function* fetchAllMerchants(): any {
    try {
        yield put(setIsLoading(true));
        const merchants = yield call(fetchMerchants, 50);

        yield put(setMerchants(merchants));
        yield put(setIsLoading(false));
    } catch (e) {
        console.error(e)
    }
}

function* watchFetchMerchants(): any {
    yield takeLatest('@@merchants/FETCH_ALL', fetchAllMerchants);
}


function* addNewMerchant(action: ReturnType<typeof actionCreateMerchant>): any {
    try {
        const merchant = yield call(createMerchant, action.payload);

        yield put(addMerchant(merchant));
    } catch (e) {
        console.error(e)
    }
}

function* watchAddNewMerchant(): any {
    yield takeLatest('@@merchants/CREATE', addNewMerchant);
}

const segmentSagas = [
    fork(watchFetchMerchants),
    fork(watchAddNewMerchant),
];

export default segmentSagas;
