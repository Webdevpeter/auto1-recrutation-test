import { all } from 'redux-saga/effects'

import merchantSagas from './merchants';

export default function* rootSaga(): any {
    yield all([
        ...merchantSagas,
    ])
}
