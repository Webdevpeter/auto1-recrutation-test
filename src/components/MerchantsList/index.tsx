import React, {useState} from 'react';
import List from '@material-ui/core/List';
import { Pagination } from '@material-ui/lab';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { push } from 'connected-react-router';

import { useAppDispatch, useAppSelector } from '../../hooks';
import { removeMerchant } from '../../store/merchantsSlice';

function MerchantsList(): JSX.Element {
    const [page, setPage] = useState(1);
    const {isLoading, merchants} = useAppSelector(state => state.merchants);
    const dispatch = useAppDispatch();
    const perPage = 10;
    const offset = page * perPage - perPage;
    const pageCount = Math.ceil(merchants?.length / perPage);

    if (pageCount < page) {
        setPage(pageCount);
    }

    return <>
        {isLoading || !merchants ? 'Loading...' : <>
            <List>
                {merchants.slice(offset, offset + perPage).map(merchant => (
                    <ListItem style={{cursor: 'pointer'}} key={`merchant-item-${merchant.id}`} onClick={() => {dispatch(push(`/merchant/${merchant.id}`))}}>
                        <ListItemAvatar>
                        <Avatar>
                            <FolderIcon />
                        </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            primary={`Id: ${merchant.id} firstname: ${merchant.firstname}`}
                        />
                        <ListItemSecondaryAction>
                        <IconButton onClick={() => {
                            dispatch(removeMerchant(merchant.id));
                        }} edge="end" aria-label="delete">
                            <DeleteIcon />
                        </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem> 
                ))}
            </List>
            <Pagination page={page} count={pageCount} onChange={(ev, page: number) => {
                setPage(page)
            }} />
        </>}
    </>
}

export default MerchantsList;