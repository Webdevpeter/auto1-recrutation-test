import webpack from "webpack";
import HtmlWebPackPlugin from "html-webpack-plugin";
import path from 'path';

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html"
});

const config = {
	mode: "development",
	entry: "./src/index.tsx",
	output: {
		publicPath: '/',
	},
	devServer: {
		historyApiFallback: true,
	},
	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: [".ts", ".tsx", ".js", ".json"]
	},
	module: {
		rules: [
		// All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
		{ test: /\.tsx?$/, loader: "ts-loader" }
		]
	},
	plugins: [htmlPlugin]
};

export default config;